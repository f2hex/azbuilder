# Using Container Instances on Azure

Create a resource group for container instances:

```
az group create --name acs-rg --location westeurope
```

Create a container from an image available on a public registry and with a public DNS name:\

```
az container \
    create --resource-group acs-rg \
    --name f2hex-builder \
    --image ubuntu:focal \
    --command-line "tail -f /dev/null" \
    --cpu 4 \
    --memory 4 \
    --dns-name-label armbian-builder-f2hex \
    --location westeurope
```

Get the public full qualified DNS name:

```
az container show \
    --resource-group acs-rg \
    --name f2hex-builder \
    --query "{FQDN:ipAddress.fqdn,ProvisioningState:provisioningState}"
```

List the running containers into the resource group:

```
az container list -g acs-rg
```

Check the logs of the running container:

```
az container logs --resource-group acs-rg  --name f2hex-builder
```

Open a shell into the running container:

```
az container exec -g acs-rg --name f2hex-builder  --exec-command "/bin/sh"
```

Delete the container and release all the associated resources (Azure billing will stop for that resource):

```
az container delete -g acs-rg --name f2hex-builder
```

## log of session
```
oot@SandboxHost-637737053275088613:/# history
    1  mg
    2  ll
    3  apt update && apt install -y mg tree mc git
    4  top
    5  cd
    6  mkdir sd
    7  ll
    8  cd sd
    9  git clone https://github.com/armbian/build
   10  cd build
   11  ./compile.sh
   12  ll debs
   13  ll output/deb
   14  ll output/debs
   15  exit
   16  history
   ```

